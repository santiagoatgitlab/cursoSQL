drop database if exists cursoSql4;
create database if not exists cursoSql4;
use cursoSql4;
-- ------------------------------------------------------------------------------------
create table usuarios (
	id int auto_increment,
    nombre varchar(60) not null,
    apellido varchar(60) not null,
    email varchar(60) not null unique,
    username varchar(60) not null unique,
    passwd varchar(255) not null,
    nacimiento date,
    primary key (id)
);

insert into usuarios (nombre,apellido,email,username,passwd) values
("Armin","Van Saavedra","avsaavedra@gmail.com","arminvan","iehiuf734tyf7yhf7ewrf7ef"),
("Daphne","Penkins","dpenkins@outlook.com","ladaphne","YGYGUyUygUyUguyGuyG8787"),
("Phoebe","Flowers","pflowers@yahoo.com","pflow","oijoii54oij5oi4j4i3"),
("Rogelio","Anderson","randersonware@hotmail.com","randersome","RFE$2v4$$F$"),
("Graciela","Rodriguez","grodri@gmail.com","gracielita","c245789ctuc45F�$TFF43tf�Vd"),
("Amadeo","Centulla","acentulla@gmail.com","amadeusamadeus","SDDF54534RGERGErtfddsldkfjhsdkjrhc"),
("Graciela","Fari�o","gfari�o@outlook.com","gracielaf","laskjdlsakdjranaskdjanskjdn");

create table albums(
	id int auto_increment,
	user_id int not null,
	titulo varchar(50),
    descripcion varchar(255),
    fecha_creacion datetime default current_timestamp,
    primary key (id)
);

insert into albums (user_id,titulo,descripcion) values 
(3,"Vacaciones en dinamarca","Un viaje maravilloso lleno de maravillas y cosas que no vas a poder creer"),
(3,"Mi depto","3 ambientes, contrafrente, muchas luz, expensas barat�simas, 55m2"),
(2,"Vacacion 2012",""),
(4,"Pinky","Todas las fotos que sacamos cuando fuimos a visitar a Juli");

-- ------------------------------------------------------------------------------------

--
-- Table structure for table `categorias`
--



CREATE TABLE `categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
);

--
-- Dumping data for table `categorias`
--

INSERT INTO `categorias` VALUES (2,'Computacion'),(5,'Electricidad'),(1,'Electrodom�sticos'),(4,'Muebles'),(6,'Telefon�a y dispositivos m�viles'),(3,'TV y hogar');

--
-- Table structure for table `marcas`
--

CREATE TABLE `marcas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
);

--
-- Dumping data for table `marcas`
--

INSERT INTO `marcas` VALUES (5,'Acer'),(6,'Asus'),(3,'Atma'),(37,'braun'),(9,'Dell'),(16,'First line'),(2,'Hewlett packard'),(14,'Huawei'),(11,'Intel'),(7,'Kingston'),(10,'LG'),(36,'microsoft'),(35,'More perfect'),(13,'Motorola'),(12,'Nvidia'),(8,'Patrick'),(1,'Samsung'),(4,'Sony'),(15,'Top house'),(17,'Very best');

--
-- Table structure for table `productos`
--

CREATE TABLE `productos_relaciones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `nuevo` tinyint(1) DEFAULT '1',
  `marca_id` int(11) NOT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

--
-- Dumping data for table `productos`
--

INSERT INTO `productos_relaciones` VALUES (1,'Televisor LED 32 pulgadas','Mir� todos los goles de Messi',1,1,3,3499.00,15),(2,'Televisor 18 pulgadas','a�o 1887, funciona b�rbaro',0,4,3,580.00,1),(3,'Smart TV full hd','42 pulgadas',1,10,3,4680.00,82),(4,'Smart TV 65 pulgadas',NULL,1,1,3,7800.00,6),(5,'LED HD','Televisor 45 pulgadas con Netflix, Youtube y Messi',1,10,3,4999.00,180),(6,'Tv 29 Pantalla plana','Pantalla slim tubo lcd outlet',1,17,3,1200.00,43),(7,'Tv LED full hd','36\" full HD 3d',1,1,NULL,5600.00,80),(8,'Memoria RAM','2GB DDR 3',1,7,2,420.00,40),(9,'Monitor 24\" ','Pantalla inteligente',1,1,2,2700.00,0),(10,'Monitor 26\" ','Pantalla flat ultra led',1,10,2,2899.00,50),(11,'Teclado USB\" ','167 teclas, luces y sonido',1,36,2,220.00,50),(12,'Motherboard',NULL,1,6,2,1230.00,14),(13,'Microprocesador i5 3.6 Ghz','multithread, 8 core, turbo',1,11,2,2450.00,10),(14,'Notebook 17\"','Intel core i7, 4GB, 1TB',1,2,2,12700.00,20),(15,'Notebook 15\"','Intel core i7, 8GB, 1TB',1,9,2,14999.00,20),(16,'Notebook 14\"','Intel atom 2800mhz, hd 500Mb, ram 4Gb',1,4,NULL,15999.00,20),(17,'Heladera 450Lts.','frio no frost. de regalo cubetera con 4 cubitos',1,8,1,6999.00,17),(18,'Heladera 400Lts.',NULL,1,8,1,5440.00,20),(19,'Batidora','para reposter�a',1,3,1,2000.00,40),(20,'Licuadora','soporta bananas y peras',1,3,1,1500.00,20),(21,'Minipimer','maquina para hacer pastas. Super oferta',1,37,1,990.00,10),(22,'multiprocesadora','arroz, carnes, empanadas',1,15,NULL,1899.00,30),(23,'Smartphone','Galaxy S4, 16GB',1,1,6,4999.00,10),(24,'Smartphone','J500',1,15,6,4200.00,15),(25,'Tablet 10\"','Wifi, whatsapp, facebook',1,4,6,3000.00,7),(26,'Tel�fono Moto G4','wifi, whatsap, blutooth',1,13,6,5200.00,40),(27,'Tel�fono Moto G3','wifi, whatsap, blutooth',1,13,6,4200.00,50),(28,'Tel�fono LG','16GB hasta. C�mara 10Mpx',1,13,NULL,5400.00,40),(29,'Pendrive','32GB',1,7,2,160.00,36),(30,'Tel�fono con wasap','T�lefono con wasap',0,4,6,1000.00,1),(31,'Telefono con wasap','Telefono con wasap',0,4,6,1000.00,1),(32,'Disco r�gido externo','2TB',1,1,2,1460.00,14);

--
-- Table structure for table `productos_2`
--

CREATE TABLE `productos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `nuevo` tinyint(1) DEFAULT '1',
  `precio` decimal(10,2) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `marca` varchar(60) DEFAULT NULL,
  `categoria` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

--
-- Dumping data for table `productos_2`
--

INSERT INTO `productos` VALUES (1,'Televisor LED 32 pulgadas','Mir� todos los goles de Messi',1,3499.00,15,'Samsung','TV y hogar'),(2,'Televisor 18 pulgadas','a�o 1887, funciona b�rbaro',0,580.00,1,'Sony','TV y hogar'),(3,'Smart TV full hd','42 pulgadas',1,4680.00,82,'LG','TV y hogar'),(4,'Smart TV 65 pulgadas',NULL,1,7800.00,6,'Samsung','TV y hogar'),(5,'LED HD','Televisor 45 pulgadas con Netflix, Youtube y Messi',1,4999.00,180,'LG','TV y hogar'),(6,'Tv 29 Pantalla plana','Pantalla slim tubo lcd outlet',1,1200.00,43,'Very best','TV y hogar'),(7,'Tv LED full hd','36\" full HD 3d',1,5600.00,80,'Samsung',NULL),(8,'Memoria RAM','2GB DDR 3',1,420.00,40,'Kingston','Computaci�n'),(9,'Monitor 24\" ','Pantalla inteligente',1,2700.00,0,'Samsung','Computaci�n'),(10,'Monitor 26\" ','Pantalla flat ultra led',1,2899.00,50,'LG','Computaci�n'),(11,'Teclado USB\" ','167 teclas, luces y sonido',1,220.00,50,'Microsoft','Computaci�n'),(12,'Motherboard',NULL,1,1230.00,14,'Asus','Computaci�n'),(13,'Microprocesador i5 3.6 Ghz','multithread, 8 core, turbo',1,2450.00,10,'Intel','Computaci�n'),(14,'Notebook 17\"','Intel core i7, 4GB, 1TB',1,12700.00,20,'Hewlett packard','Computaci�n'),(15,'Notebook 15\"','Intel core i7, 8GB, 1TB',1,14999.00,20,'Dell','Computaci�n'),(16,'Notebook 14\"','Intel atom 2800mhz, hd 500Mb, ram 4Gb',1,15999.00,20,'Sony',NULL),(17,'Heladera 450Lts.','frio no frost. de regalo cubetera con 4 cubitos',1,6999.00,17,'Patrick','Electrodom�sticos'),(18,'Heladera 400Lts.',NULL,1,5440.00,20,'Patrick','Electrodom�sticos'),(19,'Batidora','para reposter�a',1,2000.00,40,'Atma','Electrodom�sticos'),(20,'Licuadora','soporta bananas y peras',1,1500.00,20,'Atma','Electrodom�sticos'),(21,'Minipimer','maquina para hacer pastas. Super oferta',1,990.00,10,'Braun','Electrodom�sticos'),(22,'multiprocesadora','arroz, carnes, empanadas',1,1899.00,30,'Top house',NULL),(23,'Smartphone','Galaxy S4, 16GB',1,4999.00,10,'Samsung','Telefon�a y dispositivos m�viles'),(24,'Smartphone','J500',1,4200.00,15,'Top house','Telefon�a y dispositivos m�viles'),(25,'Tablet 10\"','Wifi, whatsapp, facebook',1,3000.00,7,'Sony','Telefon�a y dispositivos m�viles'),(26,'Tel�fono Moto G4','wifi, whatsap, blutooth',1,5200.00,40,'Motorola','Telefon�a y dispositivos m�viles'),(27,'Tel�fono Moto G3','wifi, whatsap, blutooth',1,4200.00,50,'Motorola','Telefon�a y dispositivos m�viles'),(28,'Tel�fono LG','16GB hasta. C�mara 10Mpx',1,5400.00,40,'LG',NULL),(29,'pendrive','32GB',1,160.00,36,'Kingston','Computaci�n'),(30,'Telefono con wasap','Telefono con wasap',0,1000.00,1,'Sony','Telefon�a y dispositivos m�viles'),(31,'Disco r�gido externo','2TB',1,1460.00,14,'Samsung','Computaci�n');

-- Ejercicios

CREATE TABLE autos (
  Id int(11) NOT NULL auto_increment,
  Marca varchar(45) NOT NULL,
  Modelo varchar(45) NOT NULL,
  Color varchar(45) DEFAULT NULL,
  Anio int(10) unsigned DEFAULT NULL,
  Precio double,
  PRIMARY KEY (Id)
);

insert into autos  (Marca,Modelo,Color,Anio,Precio)
values ("Volkswagen","Golf","Blanco",2010,210000),
("Ford","Ka","Rojo",2008,105000),
("Ford","Ecosport","Negro",2012,230000),
("Renault","Megane III","Plateado",2015,260000),
("Renault","19","Bord�",2001,80000),
("Ford","focus","Gris",2010,165000),
("Peugeot","208","Negro",2014,170000),
("Chevrolet","Cruze","Gris",2014,235000),
("Peugeot","307","Bord�",2007,115000),
("Peugeot","508","Negro",2015,320000),
("Chevrolet","Agile","Azul",2010,135000),
("Fiat","Strada","Blanco",2012,110000),
("Fiat","Duna","Blanco",1996,55000),
("Volkswagen","Sur�n","Rojo",2011,170000),
("Peugeot","408","Gris",2015,220000),
("Nissan","Tiida","Negro",2013,180000),
("Volkswagen","Amarok","Blanco",2016,340000),
("Peugeot","307","Azul",2010,140000);

create table clientes (
id int auto_increment,
nombre varchar(20) not null,
apellido varchar(20) not null,
cuit varchar(13),
direccion varchar(50),
comentarios varchar(140),
primary key (id)
);

insert into clientes (nombre,apellido,cuit,direccion) values ('Camila','hern�ndez','20-31455641-1','mompox 1395');
insert into clientes (nombre,apellido,cuit,direccion) values ('Camila','pratto','23-34554346-5','apul� 145');
insert into clientes (nombre,apellido,cuit,direccion) values ('Esteban','Perez','23-24332659-1','casacuberta 156');
insert into clientes (nombre,apellido,cuit,direccion) values ('ariana','gonz�lez','20-31467553-6','garro 1586');
insert into clientes (nombre,apellido,cuit,direccion) values ('Elias Mariano','Gonz�lez','24-34023949-3','Esquel 1945 C');
insert into clientes (nombre,apellido,cuit,direccion) values ('juan','perez','23-14565443-2','peru 323');
insert into clientes (nombre,apellido,cuit,direccion) values ('diego','torres','23-37563181-4','chile 320');
insert into clientes (nombre,apellido,cuit,direccion) values ('laura','gomez','20-13546778-1','san juan 420');
insert into clientes (nombre,apellido,cuit,direccion) values ('mario','lopez','27-15665778-1','lavalle 770');
insert into clientes (nombre,apellido,cuit,direccion) values ('dario','sanchez','27-29334610-8','mexico 150');


create table facturas(
letra char(1),
numero integer,
fecha date,
monto double,
id_cliente int,
primary key (letra,numero)
);

insert into facturas values ('a',0001,'2017/10/18',500,1);
insert into facturas values ('a',0002,'2017/10/15',2500,2);
insert into facturas values ('b',0001,'2017/10/15',320,4);
insert into facturas values ('a',0003,'2017/10/19',120,2);
insert into facturas values ('b',0002,'2017/10/14',560,4);

insert into facturas values ('a',0004,'2017/10/18',500,3);
insert into facturas values ('a',0005,'2017/10/16',2500,3);
insert into facturas values ('c',0001,'2017/10/16',620,5);
insert into facturas values ('c',0002,'2017/10/19',420,6);
insert into facturas values ('b',0003,'2017/10/14',560,7);

insert into facturas values ('d',0001,'2017/10/17',600,3);
insert into facturas values ('a',0006,'2017/10/18',2600,9);
insert into facturas values ('c',0003,'2017/10/16',620,9);
insert into facturas values ('c',0004,'2017/10/20',420,2);
insert into facturas values ('b',0004,'2017/10/14',560,10);



create table paises (
	id int auto_increment,
    nombre varchar(30),
    id_continente int,
    primary key (id)
);

create table continentes (
	id int auto_increment,
    nombre varchar(30),
    primary key (id)
);

insert into continentes (nombre) values
("Asia"),
("Am�rica"),
("Europa"),
("�frica"),
("Ocean�a"),
("Ant�rtida");

insert into paises (nombre,id_continente) values
("Argentina",2),
("Chile",2),
("Peru",2),
("Francia",3),
("Espa�a",3),
("Kenya",4),
("Tanzania",4),
("Rumania",3),
("Jap�n",1),
("Corea del norte",1),
("Corea del sur",1),
("Venezuela",2),
("Australia",null),
("India",null),
("finlandia",null);