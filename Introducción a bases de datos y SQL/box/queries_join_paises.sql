create table paises (
	id int auto_increment,
    nombre varchar(30),
    id_continente int,
    primary key (id)
);

create table continentes (
	id int auto_increment,
    nombre varchar(30),
    primary key (id)
);

insert into continentes (nombre) values
("Asia"),
("América"),
("Europa"),
("África"),
("Oceanía"),
("Antártida");

insert into paises (nombre,id_continente) values
("Argentina",2),
("Chile",2),
("Peru",2),
("Francia",3),
("España",3),
("Kenya",4),
("Tanzania",4),
("Rumania",3),
("Japón",1),
("Corea del norte",1),
("Corea del sur",1),
("Venezuela",2),
("Australia",null),
("India",null),
("finlandia",null);