Ejercicios sobre la tabla `autos`

3. Realizar las siguientes consultas:
a. obtener el precio m�ximo.
b. obtener el precio m�nimo.
c. obtener el precio m�nimo entre los a�os 2008 y 2013.
d. obtener el precio promedio.
e. obtener el precio promedio del a�o 2010.
f. obtener la cantidad de autos.
g. obtener la cantidad de autos que tienen un precio entre $90.000 y $120.000.
h. obtener la cantidad de autos por cada a�o.
i. obtener la cantidad de autos y el precio promedio por a�o.
j. obtener la suma de precios y el promedio de precios seg�n marca.

Ejercicio sobre la tablas `clientes` y `facturas` usando join

a. Informar que clientes han comprado en el d�a de hoy.
b. Informar la suma de los montos de cada cliente.
c. Informar la cantidad de facturas de cada cliente.
d. Informar quienes compraron el primer d�a de ventas.
e. Informar que compr� el cliente Juan Perez.