/*
COMANDO ALTER TABLE: Introduce modificaciones en la estructura de una tabla ya creada.

Primero creamos una tabla para poder aplicar los ejemplos del comando
*/

create table productos(
	idProducto int auto_increment primary key,
    nombre varchar(30) not null,
    precio double not null,
    marca varchar(30) not null,
    rubro varchar(20) not null,
    descripcion varchar(30) not null,
    stock int not null
);


--
-- Ejemplo 1: Usamos CHANGE para modificar un campo ya existente
--

ALTER TABLE productos CHANGE rubro categor�a varchar(20) not null;

/*
Es importante notar que esta consulta en realidad mas que modificar un campo existente lo que hace es darlo de baja y crear uno nuevo en la misma posici�n.
En este caso da de baja el campo rubro y en la misma posici�n crea un campo categor�a. Por eso es que tenemos que volver a definir su tipo y sus atributos (en este caso not null)
*/

--
-- Ejemplo 2: Usamos ADD para agregar un campo nuevo
--

ALTER TABLE productos ADD disponible boolean;

-- Agrega un campo llamado `disponible`, del tipo boolean, al final de la tabla 


--
-- Ejemplo 3: Usamos ADD para eliminar un campo de la tabla
--

ALTER TABLE productos DROP disponible;

-- Elimina el campo llamado `disponible` de la tabla

