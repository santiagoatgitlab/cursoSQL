drop database if exists cursosql3;
create database cursosql3;
use cursosql3;


-- Eliminamos posibles tablas que existan con los nombres de las tablas que vamos a crear para que al 
-- ejecutar create no de error de tabla ya existente
drop table if exists clientes;


create table clientes (
id integer auto_increment,
nombre varchar(20) not null,
apellido varchar(20) not null,
cuit varchar(13),
direccion varchar(50),
comentarios varchar(140),
primary key (id	)
);

insert into clientes (nombre,apellido,cuit,direccion) values ('Camila','hern�ndez','20-31455641-1','mompox 1395');
insert into clientes (nombre,apellido,cuit,direccion) values ('Camila','pratto','23-34554346-5','apul� 145');
insert into clientes (nombre,apellido,cuit,direccion) values ('Esteban','Perez','23-24332659-0','casacuberta 156');
insert into clientes (nombre,apellido,cuit,direccion,comentarios) values ('ariana','gonz�lez','20-31467553-6','garro 1586','tiene un hijo arquitecto');
insert into clientes (nombre,apellido,cuit,direccion) values ('Elias Mariano','Gonz�lez','24-34023949-3','Esquel 1945 C');
insert into clientes (nombre,apellido,cuit,direccion) values ('juan','perez prado','23-14565443-2','peru 323');
insert into clientes (nombre,apellido,cuit,direccion) values ('diego','torres','23-37563181-0','chile 320');
insert into clientes (nombre,apellido,cuit,direccion) values ('laura','g�mez','20-13546778-1','san juan 420');
insert into clientes (nombre,apellido,cuit,direccion,comentarios) values ('mario','l�pez','27-15665778-1','lavalle 770','Esta viviendo en jap�n');
insert into clientes (nombre,apellido,cuit,direccion) values ('dario','sanchez','27-29334610-8','mexico 150');

truncate clientes;

create table articulos(
id int auto_increment,
nombre varchar(50),
precio double,
stock integer,
primary key (id)
);

insert into articulos (nombre, precio, stock) values ('cable de red',30,50);
insert into articulos (nombre, precio, stock) values ('memoria SD',140,22);
insert into articulos (nombre, precio, stock) values ('pendrive',80,28);
insert into articulos (nombre, precio, stock) values ('cargador celular',60,0);
insert into articulos (nombre, precio, stock) values ('teclado logitech',180,13);
insert into articulos (nombre, precio, stock) values ('pila',40,13);
insert into articulos (nombre, precio, stock) values ('cable USB',40,0);
insert into articulos (nombre, precio, stock) values ('cable USB',40,13);

create table empleados(
        codigo int auto_increment,
        nombre varchar(20) not null,
        apellido varchar(20) not null,
        seccion varchar(20),
        sueldo float,
        primary key (codigo)
);

insert into empleados (nombre,apellido,seccion,sueldo) values
        ('juan','perez','administracion',7200),
        ('diego','torres','ventas',3520),
        ('laura','gomez','ventas',4600),
        ('mario','lopez','produccion',4500),
        ('dario','sanchez','ventas',8600),
        ('juan','boneli','administracion',7200),
        ('diego','martinez','ventas',3520),
        ('susana','moretti','ventas',4600),
        ('sandra','lazante','produccion',4500),
        ('susana','mendez','administracion',8600);

		/*
insert into empleados (nombre,apellido,seccion_id,sueldo) values
        ('juan','perez',1,7200),
        ('diego','torres',3,3520),
        ('laura','gomez',3,4600),
        ('mario','lopez',2,4500),
        ('dario','sanchez',3,8600),
        ('juan','boneli',1,7200),
        ('diego','martinez',3,3520),
        ('susana','moretti',3,4600),
        ('sandra','lazante',2,4500),
        ('susana','mendez',1,8600);
*/
create table usuarios (
	id int auto_increment,
    nombre varchar(60) not null,
    apellido varchar(60) not null,
    email varchar(60) not null unique,
    username varchar(60) not null unique,
    passwd varchar(255) not null,
    nacimiento date,
    primary key (id)
);

insert into usuarios (nombre,apellido,email,username,passwd) values
("Armin","Van Saavedra","avsaavedra@gmail.com","arminvan","iehiuf734tyf7yhf7ewrf7ef"),
("Daphne","Penkins","dpenkins@outlook.com","ladaphne","YGYGUyUygUyUguyGuyG8787"),
("Phoebe","Flowers","pflowers@yahoo.com","pflow","oijoii54oij5oi4j4i3"),
("Rogelio","Anderson","randersonware@hotmail.com","randersome","RFE$2v4$$F$"),
("Graciela","Rodriguez","grodri@gmail.com","gracielita","c245789ctuc45F�$TFF43tf�Vd"),
("Amadeo","Centulla","acentulla@gmail.com","amadeusamadeus","SDDF54534RGERGErtfddsldkfjhsdkjrhc"),
("Graciela","Fari�o","gfari�o@outlook.com","gracielaf","laskjdlsakdjranaskdjanskjdn");

/*
insert into albums (user_id,titulo,descripcion) values 
(3,"Vacaciones en dinamarca","Un viaje maravilloso lleno de maravillas y cosas que no vas a poder creer"),
(3,"Mi depto","3 ambientes, contrafrente, muchas luz, expensas barat�simas, 55m2"),
(2,"Vacacion 2012",""),
(4,"Pinky","Todas las fotos que sacamos cuando fuimos a visitar a Juli");
*/
