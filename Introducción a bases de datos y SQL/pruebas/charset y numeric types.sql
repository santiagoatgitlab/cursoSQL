show databases; -- alksdjalskdj
use comercioIt;
show tables;
describe ALL_PLUGINS;
describe pma__bookmark;
set sql_safe_updates = 0;

create table charsetTest (
	nombre1 VARCHAR(10) CHARSET utf8 COLLATE utf8_general_ci,
    nombre2 VARCHAR(10) CHARSET latin1 COLLATE latin1_general_cs,
    nombre3 VARCHAR(10) CHARSET ascii COLLATE ascii_general_ci
);

drop table charsetTest;
show tables;

select * from numtest;
select length(num_float),length(num_double),length(num_decimal),length(num_int),length(num_float_) from numtest;

insert into charsetTest (nombre1,nombre2,nombre3,nombre4) VALUES ("ñandú","ñandú","ñandú","ñandú");
select * from charsetTest;
alter table charsetTest add nombre4 varchar(60) charset utf8 collate utf8_unicode_ci;
insert into charsetTest (nombre1,nombre2,nombre3) VALUES ("сантьиаго","сантьиаго","сантьиаго");

alter table numtest add num_float_ float;
insert into numtest (num_float_) values ("012345678901234567");

create table articulos(
	nombre varchar(20),
    precio float,
    precio_d double,
    precio_dc decimal(8,2)
);

insert into articulos VALUES ("lavarropas",157543.23,157543.23,157543.23);
select length(precio),length(precio_d),length(precio_dc) from articulos;
select * from articulos;

rename table articulos to productos;

select * from charsetTest WHERE nombre1 like "Ñ%";
alter table charsetTest change nombre4 nombre4 varchar(10) charset utf8 collate utf8_bin;

select * from clientes;


insert into clientes (nombre, apellido, edad, domicilio, género) VALUES ("juan", "lópez", 34, "rivadavia 14", "m");
insert into clientes (nombre, apellido, edad, domicilio, género) VALUES ("juan", "Ramirez", 23, "Quentin 123", "m");
insert into clientes (nombre, apellido, edad, domicilio, género) VALUES ("Lorena", "Laigo", 33, "Quentin 543", "f");
insert into clientes (nombre, apellido, edad, domicilio, género) VALUES ("David", "Giovanni", 28, "Coronel pagoya 1238", "m");
insert into clientes (nombre, apellido, edad, domicilio, género) VALUES ("camila", "Giovanni", 20, "Rua dos santos 48", "m");
select distinct nombre, apellido, edad from clientes;
select nombre, apellido from clientes;

select 2 + 2 as pochoclo,nombre,apellido from clientes;
select * from clientes,facturas;
select count(*) from clientes join facturas;

