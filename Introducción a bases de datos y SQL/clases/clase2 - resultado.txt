-Comando SELECT
-* y Alias
-columnas calculadas (IVA)
-WHERE (operadores de comparación y operadores lógicos)
-Between, not between, like, not like, in, not in
-order by, limit

---------------------------------------------------------------------------------------------------

-Repaso con ejemplos de claves UNIQUE y PRIMARY KEY
-Como les fué? Pudieron instalar? recomendaciones (probar mucho código, tratar de interpretar los mensajes de error)
-Explicación de lenguages DDL y DML con repaso de conceptos de registros y campos
	Diferenciar entre los comandos que alteran el contenido de la base de datos (registros) de los que solo alteran las estructura
-Comando select
	Ejemplos de seleccionar solo 1 campo
		select letra,monto from facturas
	Ejemplos de seleccionar 1 campo con operaciones matemáticas
		select monto * 2 from facturas
	Ejemplo de Alias
		select monto, monto * 1.21 as "monto + iva" from facturas
-WHERE
	Ejemplos con = (texto y numero)
		select from articulos where nombre = "camila"		
	Ejemplos con =,>,<,>=,<=,!=		
		select * from articulos where precio = 60
		select * from articulos where precio > 60
		select * from articulos where precio < 60
		select * from articulos where precio >= 60
		select * from articulos where precio <= 60
		select * from articulos where precio != 40
	AND y OR
		ejemplos con los nombres y apellidos
		ejemplos con rangos de numeros
	Insertar clientes con domicilio null y domicilio vacío
		insert into clientes (nombre, apellido, cuit, direccion) values ("mariano","pereyra","20-12324556-1","");
		insert into clientes (nombre, apellido, cuit, direccion) values ("mariana","rodriguez","24-23324556-4",null);
	Ejemplos con is null,is not null
		select * from clientes where direccion is null;
		select * from clientes where direccion is not null;
-Columnas calculadas y con alias
-Mas WHERE
	Ejemplos de Between y not between (+ ejemplos de la vida real, por ejemplo mercadolibre)
		select * from articulos where precio between 40 and 80;
		select * from articulos where precio not between 40 and 80;
	Como hacer lo mismo con >,<,and,or	
	Ejemplos de In y not in (+ ejemplos de la vida real, por ejemplo mercadolibre)
		select * from clientes where codigo in (2,5,6,7);
		select * from clientes where codigo not in (2,5,6,7);
	Como hacer lo mismo con =,and,or	
	Ejemplos de Like y not like (+ ejemplos de la vida real, por ejemplo busquedas de youtube)
		select * from clientes where nombre like "%an%";
		select * from clientes where nombre like "%an";
		select * from clientes where nombre like "__an";
		select * from clientes where nombre like "d%";
		select * from clientes where apellido like "g%";
		select * from clientes where nombre like "%i%n%";
		
-Order by y limit

	ordenar por nombre
	ordenar por nombre descendiente
	odrenar por nombre, apellido
	limit(offset, row_count)

Ejercicios



Ejercicio 1

	Llenar las tablas artículos clientes y facturas con 10 registros cada una
	
Ejercicio 2	

	Basándose en la tabla artículos obtener los siguientes listados.
	artículos con precio mayor a 100
	artículos con precio entre 20 y 40 (usar < y >)
	artículos con precio entre 40 y 60 (usar BETWEEN)
	artículos con precio = 220 y stock menor a 20
	artículos con precio (12,30,60) no usar IN
	artículos con precio (12,30,60) usar el IN
	artículos que su precio no sea (12,30,60)
	
Ejercicio 3

	Listar los artículos ordenados por precio de mayor a menor, y si hubiera precio iguales deben quedar ordenados por nombre.
	
Ejercicio 4

	Listar todos los artículos incluyendo una columna denominada precio con IVA, la cual deberá tener el monto con el iva del producto.
	
Ejercicion 5

	Listar todos los artículos incluyendo una columna denominada “cantidad de cuotas” y otra “valor de cuota”, la cantidad es fija y es 3, el valor de cuota corresponde a 1/3 del monto con un 5% de interés.
	
