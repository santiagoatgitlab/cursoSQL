Instruducción
 
	-Estan todos para bases de datos
	-Mi nombre, en que trabajo
	-¿que carreras cursan? ¿que nociones tienen de bases de datos?
	-¿Saben como funciona el alumni?

Bases de datos

	-Que es una base de datos (en un sentido global)
	-Gráfico interfaz, procesamiento, almacenamiento
		-Ejemplos de distintas interfaces en una misma base de datos
	-Explicación de tablas y comparación con una planilla de excel
	-Servidor y cliente	
	-Una base de datos en un sentido mas técnico.
		-Servidor, bases de datos, tablas
	-Que es SQL
	-Motores de bases de datos
		-Oracle
		-PostrgreSQL
		-SQL server
		-MySQL
		-MariaDB
	-Xampp
	-Workbench
	-Conexión
	
IMPORTANTE: Antes de empezar a trabajar, hacer enfasis en que instalen en sus casas y en que se lleven el código de la clase
	
01.Primeros comandos (show, create database, use)
02.Explicación de comentarios
03.Recomendación de guardar y llevarse el código
04.comando create database
04.comando use
05.Comando create table (clientes: nombre, apellido, edad, domicilio, género)
06.Explicación de tipos de datos
	Numericos
		tiny,small,medium,int,long
	Decimales
		float,double,decimal
	Texto
		char,varchar
	otros 
		date, datetime, time, year, boolean
		
07.Comando describe
08.insert básico
09.select básico

10.Explicación de Atributos y clave primaria
	Not null,
	Auto_increment,
	Unique,	
	Primery key
	
11.Creación de tablas con atributos 
	productos: 
		idProducto int 11 nn ai pk,
		nombre: varchar 30 nn,
		precio: double nn
		marca: varchar 20 nn
		rubro: varchar 20 nn
		presentacion: varchar 30 nn
		stock int nn
		
12.Alterar tabla (cambiar campos)
13.Alterar tabla (agregar campos) 
14.Alterar table (eliminar campos)

Attributos default