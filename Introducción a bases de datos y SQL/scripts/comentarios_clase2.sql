# SELECT 

	# Especificando campos (tabla facturas: letra, monto)

	# Nombres de campos inexistentes (error)

	# Valores ingresados en el momento (palabras, numeros)

	# Operaciones matemáticas (monto)

	# Alias (Ejemplo del IVA: monto)

# WHERE (articulos)

	# nombre "camila"

	# precio =, > y < que 60 (3 queries)	

	# precio >= y <= que 60 (2 queries)

	# precio != a 40

# AND y OR (tabla clientes)
 
	# nombres y apellidos

	# rangos de números

# Vacío y NULL

	# Insertar un registro con dirección vacía y otro con dirección null

	# select con dirección vacía

	# selecto con dirección nula

# BETWEEN, IN, LIKE 

	# Between 40 y 80 (tabla artículos)
	
	# In 2, 5, 6, 7

	# LIKE (%an%, %an, __an, d%, g%, %i%n%)

# Operador NOT
	
	# Ejemplos de todo lo mismo pero con NOT (Between, in, LIKE)

# ORDER BY

	# Ordenar por nombre

	# Ordenar por nombre descendiente

	# Ordenar por apellido

	# Ordenar por nombre, apellido
