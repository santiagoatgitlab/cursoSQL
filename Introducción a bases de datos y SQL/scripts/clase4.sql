drop database if exists cursoSql4;
create database if not exists cursoSql4;
use cursoSql4;
-- ------------------------------------------------------------------------------------

--
-- Table structure for table `categorias`
--

CREATE TABLE `categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
);

--
-- Dumping data for table `categorias`
--

INSERT INTO `categorias` VALUES (2,'Computacion'),(5,'Electricidad'),(1,'Electrodomésticos'),(4,'Muebles'),(6,'Telefonía y dispositivos móviles'),(3,'TV y hogar');

--
-- Table structure for table `marcas`
--

CREATE TABLE `marcas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
);

--
-- Dumping data for table `marcas`
--

INSERT INTO `marcas` VALUES (5,'Acer'),(6,'Asus'),(3,'Atma'),(37,'braun'),(9,'Dell'),(16,'First line'),(2,'Hewlett packard'),(14,'Huawei'),(11,'Intel'),(7,'Kingston'),(10,'LG'),(36,'microsoft'),(35,'More perfect'),(13,'Motorola'),(12,'Nvidia'),(8,'Patrick'),(1,'Samsung'),(4,'Sony'),(15,'Top house'),(17,'Very best');

--
-- Table structure for table `productos`
--

CREATE TABLE `productos_relaciones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `nuevo` tinyint(1) DEFAULT '1',
  `marca_id` int(11) NOT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

--
-- Dumping data for table `productos`
--

INSERT INTO `productos_relaciones` VALUES (1,'Televisor LED 32 pulgadas','Mirá todos los goles de Messi',1,1,3,3499.00,15),(2,'Televisor 18 pulgadas','año 1887, funciona bárbaro',0,4,3,580.00,1),(3,'Smart TV full hd','42 pulgadas',1,10,3,4680.00,82),(4,'Smart TV 65 pulgadas',NULL,1,1,3,7800.00,6),(5,'LED HD','Televisor 45 pulgadas con Netflix, Youtube y Messi',1,10,3,4999.00,180),(6,'Tv 29 Pantalla plana','Pantalla slim tubo lcd outlet',1,17,3,1200.00,43),(7,'Tv LED full hd','36\" full HD 3d',1,1,NULL,5600.00,80),(8,'Memoria RAM','2GB DDR 3',1,7,2,420.00,40),(9,'Monitor 24\" ','Pantalla inteligente',1,1,2,2700.00,0),(10,'Monitor 26\" ','Pantalla flat ultra led',1,10,2,2899.00,50),(11,'Teclado USB\" ','167 teclas, luces y sonido',1,36,2,220.00,50),(12,'Motherboard',NULL,1,6,2,1230.00,14),(13,'Microprocesador i5 3.6 Ghz','multithread, 8 core, turbo',1,11,2,2450.00,10),(14,'Notebook 17\"','Intel core i7, 4GB, 1TB',1,2,2,12700.00,20),(15,'Notebook 15\"','Intel core i7, 8GB, 1TB',1,9,2,14999.00,20),(16,'Notebook 14\"','Intel atom 2800mhz, hd 500Mb, ram 4Gb',1,4,NULL,15999.00,20),(17,'Heladera 450Lts.','frio no frost. de regalo cubetera con 4 cubitos',1,8,1,6999.00,17),(18,'Heladera 400Lts.',NULL,1,8,1,5440.00,20),(19,'Batidora','para repostería',1,3,1,2000.00,40),(20,'Licuadora','soporta bananas y peras',1,3,1,1500.00,20),(21,'Minipimer','maquina para hacer pastas. Super oferta',1,37,1,990.00,10),(22,'multiprocesadora','arroz, carnes, empanadas',1,15,NULL,1899.00,30),(23,'Smartphone','Galaxy S4, 16GB',1,1,6,4999.00,10),(24,'Smartphone','J500',1,15,6,4200.00,15),(25,'Tablet 10\"','Wifi, whatsapp, facebook',1,4,6,3000.00,7),(26,'Teléfono Moto G4','wifi, whatsap, blutooth',1,13,6,5200.00,40),(27,'Teléfono Moto G3','wifi, whatsap, blutooth',1,13,6,4200.00,50),(28,'Teléfono LG','16GB hasta. Cámara 10Mpx',1,13,NULL,5400.00,40),(29,'Pendrive','32GB',1,7,2,160.00,36),(30,'Teléfono con wasap','Télefono con wasap',0,4,6,1000.00,1),(31,'Telefono con wasap','Telefono con wasap',0,4,6,1000.00,1),(32,'Disco rígido externo','2TB',1,1,2,1460.00,14);

--
-- Table structure for table `productos_2`
--

CREATE TABLE `productos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `nuevo` tinyint(1) DEFAULT '1',
  `precio` decimal(10,2) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `marca` varchar(60) DEFAULT NULL,
  `categoria` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

--
-- Dumping data for table `productos_2`
--

INSERT INTO `productos` VALUES (1,'Televisor LED 32 pulgadas','Mirá todos los goles de Messi',1,3499.00,15,'Samsung','TV y hogar'),(2,'Televisor 18 pulgadas','año 1887, funciona bárbaro',0,580.00,1,'Sony','TV y hogar'),(3,'Smart TV full hd','42 pulgadas',1,4680.00,82,'LG','TV y hogar'),(4,'Smart TV 65 pulgadas',NULL,1,7800.00,6,'Samsung','TV y hogar'),(5,'LED HD','Televisor 45 pulgadas con Netflix, Youtube y Messi',1,4999.00,180,'LG','TV y hogar'),(6,'Tv 29 Pantalla plana','Pantalla slim tubo lcd outlet',1,1200.00,43,'Very best','TV y hogar'),(7,'Tv LED full hd','36\" full HD 3d',1,5600.00,80,'Samsung',NULL),(8,'Memoria RAM','2GB DDR 3',1,420.00,40,'Kingston','Computación'),(9,'Monitor 24\" ','Pantalla inteligente',1,2700.00,0,'Samsung','Computación'),(10,'Monitor 26\" ','Pantalla flat ultra led',1,2899.00,50,'LG','Computación'),(11,'Teclado USB\" ','167 teclas, luces y sonido',1,220.00,50,'Microsoft','Computación'),(12,'Motherboard',NULL,1,1230.00,14,'Asus','Computación'),(13,'Microprocesador i5 3.6 Ghz','multithread, 8 core, turbo',1,2450.00,10,'Intel','Computación'),(14,'Notebook 17\"','Intel core i7, 4GB, 1TB',1,12700.00,20,'Hewlett packard','Computación'),(15,'Notebook 15\"','Intel core i7, 8GB, 1TB',1,14999.00,20,'Dell','Computación'),(16,'Notebook 14\"','Intel atom 2800mhz, hd 500Mb, ram 4Gb',1,15999.00,20,'Sony',NULL),(17,'Heladera 450Lts.','frio no frost. de regalo cubetera con 4 cubitos',1,6999.00,17,'Patrick','Electrodomésticos'),(18,'Heladera 400Lts.',NULL,1,5440.00,20,'Patrick','Electrodomésticos'),(19,'Batidora','para repostería',1,2000.00,40,'Atma','Electrodomésticos'),(20,'Licuadora','soporta bananas y peras',1,1500.00,20,'Atma','Electrodomésticos'),(21,'Minipimer','maquina para hacer pastas. Super oferta',1,990.00,10,'Braun','Electrodomésticos'),(22,'multiprocesadora','arroz, carnes, empanadas',1,1899.00,30,'Top house',NULL),(23,'Smartphone','Galaxy S4, 16GB',1,4999.00,10,'Samsung','Telefonía y dispositivos móviles'),(24,'Smartphone','J500',1,4200.00,15,'Top house','Telefonía y dispositivos móviles'),(25,'Tablet 10\"','Wifi, whatsapp, facebook',1,3000.00,7,'Sony','Telefonía y dispositivos móviles'),(26,'Teléfono Moto G4','wifi, whatsap, blutooth',1,5200.00,40,'Motorola','Telefonía y dispositivos móviles'),(27,'Teléfono Moto G3','wifi, whatsap, blutooth',1,4200.00,50,'Motorola','Telefonía y dispositivos móviles'),(28,'Teléfono LG','16GB hasta. Cámara 10Mpx',1,5400.00,40,'LG',NULL),(29,'pendrive','32GB',1,160.00,36,'Kingston','Computación'),(30,'Telefono con wasap','Telefono con wasap',0,1000.00,1,'Sony','Telefonía y dispositivos móviles'),(31,'Disco rígido externo','2TB',1,1460.00,14,'Samsung','Computación');

-- Ejercicios

CREATE TABLE autos (
  Id int(11) NOT NULL auto_increment,
  Marca varchar(45) NOT NULL,
  Modelo varchar(45) NOT NULL,
  Color varchar(45) DEFAULT NULL,
  Anio int(10) unsigned DEFAULT NULL,
  Precio double,
  PRIMARY KEY (Id)
);

insert into autos  (Marca,Modelo,Color,Anio,Precio)
values ("Volkswagen","Golf","Blanco",2010,21000),
("Ford","Ka","Rojo",2008,105000),
("Ford","Ecosport","Negro",2012,230000),
("Renault","Megane III","Plateado",2015,260000),
("Renault","19","Bordó",2001,80000),
("Ford","focus","Gris",2010,165000),
("Peugeot","208","Negro",2014,170000),
("Chevrolet","Cruze","Gris",2014,235000),
("Peugeot","307","Bordó",2007,115000),
("Peugeot","508","Negro",2015,320000),
("Chevrolet","Agile","Azul",2010,135000),
("Fiat","Strada","Blanco",2012,110000),
("Fiat","Duna","Blanco",1996,55000),
("Volkswagen","Surán","Rojo",2011,170000),
("Peugeot","408","Gris",2015,220000),
("Nissan","Tiida","Negro",2013,180000),
("Volkswagen","Amarok","Blanco",2016,340000),
("Peugeot","307","Azul",2010,140000);

create table clientes (
id int auto_increment,
nombre varchar(20) not null,
apellido varchar(20) not null,
cuit varchar(13),
direccion varchar(50),
comentarios varchar(140),
primary key (id)
);

insert into clientes (nombre,apellido,cuit,direccion) values ('Camila','hernández','20-31455641-1','mompox 1395');
insert into clientes (nombre,apellido,cuit,direccion) values ('Camila','pratto','23-34554346-5','apulé 145');
insert into clientes (nombre,apellido,cuit,direccion) values ('Esteban','Perez','23-24332659-1','casacuberta 156');
insert into clientes (nombre,apellido,cuit,direccion) values ('ariana','gonzález','20-31467553-6','garro 1586');
insert into clientes (nombre,apellido,cuit,direccion) values ('Elias Mariano','González','24-34023949-3','Esquel 1945 C');
insert into clientes (nombre,apellido,cuit,direccion) values ('juan','perez','23-14565443-2','peru 323');
insert into clientes (nombre,apellido,cuit,direccion) values ('diego','torres','23-37563181-4','chile 320');
insert into clientes (nombre,apellido,cuit,direccion) values ('laura','gomez','20-13546778-1','san juan 420');
insert into clientes (nombre,apellido,cuit,direccion) values ('mario','lopez','27-15665778-1','lavalle 770');
insert into clientes (nombre,apellido,cuit,direccion) values ('dario','sanchez','27-29334610-8','mexico 150');


create table facturas(
letra char(1),
numero integer,
fecha date,
monto double,
id_cliente int,
primary key (letra,numero)
);

insert into facturas values ('a',0001,'2017/09/18',500,1);
insert into facturas values ('a',0002,'2017/09/15',2500,2);
insert into facturas values ('b',0001,'2017/09/15',320,4);
insert into facturas values ('a',0003,'2017/09/19',120,2);
insert into facturas values ('b',0002,'2017/09/21',560,4);

insert into facturas values ('a',0004,'2017/09/18',500,3);
insert into facturas values ('a',0005,'2017/09/16',2500,3);
insert into facturas values ('c',0001,'2017/09/16',620,5);
insert into facturas values ('c',0002,'2017/09/19',420,6);
insert into facturas values ('b',0003,'2017/09/21',560,7);

insert into facturas values ('d',0001,'2017/09/17',600,3);
insert into facturas values ('a',0006,'2017/09/18',2600,9);
insert into facturas values ('c',0003,'2017/09/16',620,9);
insert into facturas values ('c',0004,'2017/09/20',420,2);
insert into facturas values ('b',0004,'2017/09/21',560,10);


select * from productos;
select *,max(precio),min(precio) from productos;
select count(*) as total from productos;
select descripcion,count(descripcion) from productos group by descripcion;


create table personas(
	nombre varchar(30),
    apellido varchar(20),
    dni varchar(9),
    edad int
);

insert into personas () values ();

select * from personas;

select count(*) from personas;
select count(nombre) from personas;
select count(apellido) from personas;
select count(dni) from personas;
select count(edad) from personas;
select count(distinct (categoria)) from productos;
select count(categoria) from productos;
select categoria from productos group by categoria;


select avg(nombre) from productos;

create table paises (
	id int auto_increment,
    nombre varchar(30),
    id_continente int,
    primary key (id)
);

create table continentes (
	id int auto_increment,
    nombre varchar(30),
    primary key (id)
);

insert into continentes (nombre) values
("Asia"),
("América"),
("Europa"),
("África"),
("Oceanía"),
("Antártida");

select * from continentes;
truncate paises;
insert into paises (nombre,id_continente) values
("Argentina",2),
("Chile",2),
("Peru",2),
("Francia",3),
("España",3),
("Kenya",4),
("Tanzania",4),
("Rumania",3),
("Japón",1),
("Corea del norte",1),
("Corea del sur",1),
("Venezuela",2),
("Australia",null),
("India",null),
("finlandia",null);

select * from paises;

select paises.nombre as pais, continentes.nombre as continente from paises,continentes where paises.id_continente = continentes.id;
insert into paises (nombre) values ("australia");
use cursosql4;
select * from paises,continentes;
insert into continentes (nombre) values ("antártida");
select * from paises left join continentes on paises.id_continente = continentes.id;

alter table paises
add constraint paises_continentes
foreign key (id_continente)
references continentes(id)
on update cascade
on delete cascade;

select * from paises;
select * from continentes;
delete from continentes where id = 7;
update paises set id_continente = 5 where id = 13;

alter table paises drop foreign key paises_continentes;

alter table paises
add constraint paises_continentes
foreign key (id_continente)
references continentes(id);

select * from continentes c 
join paises p
on p.id_continente = c.id;

delete from continentes where id = 5;

describe paises;
describe continentes;

alter table paises
add constraint nombre_unico
unique (nombre);

drop index nombre_unico on paises;

alter table productos_relaciones change cantidad stock int;

select max(precio),categoria from productos group by categoria;
select min(precio),categoria from productos group by categoria;

use cursoSql4;
select sum(stock) as total_stock, p.nombre,p.descripcion,p.precio,m.nombre as marca,c.nombre as categoria from productos_relaciones p
join marcas m on p.marca_id = m.id
join categorias c on p.categoria_id = c.id
where precio > 1000
group by marca
having total_stock > 40
order by nombre desc;


select * from productos;
