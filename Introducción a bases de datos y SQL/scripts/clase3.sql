show databases;
create database redSocial;
use redSocial;


create table usuarios (
	id int auto_increment,
    nombre varchar(60) not null,
    apellido varchar(60) not null,
    email varchar(60) not null unique,
    username varchar(60) not null unique,
    passwd varchar(255) not null,
    nacimiento date,
    primary key (id)
);

create table albums(
	id int auto_increment,
	user_id int not null,
	titulo varchar(50),
    descripcion varchar(255),
    fecha_creacion datetime default current_timestamp,
    primary key (id)
);

create table fotos(
	id int auto_increment,
    album_id int not null,
    comentario varchar(255),
    formato varchar(4),
    ruta_archivo varchar(127),
    fecha_creacion datetime default current_timestamp,
    primary key (id)
);
drop table albums, fotos;
show tables;
describe usuarios;

alter table usuarios change nacimiento fecha_nacimiento date;
alter table usuarios add edad tinyint unsigned;
alter table usuarios drop edad;

insert into usuarios (nombre,apellido,email,username,passwd) values
("Armin","Van Saavedra","avsaavedra@gmail.com","arminvan","iehiuf734tyf7yhf7ewrf7ef"),
("Daphne","Penkins","dpenkins@outlook.com","ladaphne","YGYGUyUygUyUguyGuyG8787"),
("Phoebe","Flowers","pflowers@yahoo.com","pflow","oijoii54oij5oi4j4i3"),
("Rogelio","Anderson","randersonware@hotmail.com","randersome","RFE$2v4$$F$"),
("Graciela","Rodriguez","grodri@gmail.com","gracielita","c245789ctuc45F·$TFF43tf·Vd"),
("Amadeo","Centulla","acentulla@gmail.com","amadeusamadeus","SDDF54534RGERGErtfddsldkfjhsdkjrhc"),
("Graciela","Fariño","gfariño@outlool.com","gracielaf","laskjdlsakdjranaskdjanskjdn");

select * from usuarios;

insert into albums (user_id,titulo,descripcion) values 
(3,"Vacaciones en dinamarca","Un viaje maravilloso lleno de maravillas y cosas que no vas a poder creer"),
(3,"Mi depto","3 ambientes, contrafrente, muchas luz, expensas baratísimas, 55m2"),
(2,"Vacacion 2012",""),
(4,"Pinky","Todas las fotos que sacamos cuando fuimos a visitar a Juli");

select * from albums;

select * from usuarios WHERE id IN (SELECT user_id FROM albums WHERE titulo LIKE "%Vacacion%" );
select * from albums WHERE user_id = (SELECT id FROM usuarios WHERE email = "pflowers@yahoo.com");

select * from usuarios u 
JOIN albums a ON u.id = a.user_id
WHERE a.titulo LIKE "%Vacacion%";



use cursomysql3;
select * from clientes;

select * from clientes;

update clientes set nombre = "josé" where id = 1;
update clientes set nombre = "Pablo", apellido = "Fuentes", cuit = "20-21053119-0" where id = 3;
set sql_safe_updates = 0;
update clientes set comentarios = "" where comentarios is null;

insert into clientes (nombre,apellido,cuit,direccion) values ('Camila','hernández','20-31455641-1','mompox 1395');
insert into clientes (nombre,apellido,cuit,direccion) values ('Camila','pratto','23-34554346-5','apulé 145');
insert into clientes (nombre,apellido,cuit,direccion) values ('Esteban','Perez','23-24332659-0','casacuberta 156');
insert into clientes (nombre,apellido,cuit,direccion,comentarios) values ('ariana','gonzález','20-31467553-6','garro 1586','tiene un hijo arquitecto');
insert into clientes (nombre,apellido,cuit,direccion) values ('Elias Mariano','González','24-34023949-3','Esquel 1945 C');
insert into clientes (nombre,apellido,cuit,direccion) values ('juan','perez prado','23-14565443-2','peru 323');
insert into clientes (nombre,apellido,cuit,direccion) values ('diego','torres','23-37563181-0','chile 320');
insert into clientes (nombre,apellido,cuit,direccion) values ('laura','gomez','20-13546778-1','san juan 420');
insert into clientes (nombre,apellido,cuit,direccion,comentarios) values ('mario','lopez','27-15665778-1','lavalle 770','Esta viviendo en japón');
insert into clientes (nombre,apellido,cuit,direccion) values ('dario','sanchez','27-29334610-8','mexico 150');

truncate clientes;

delete from clientes where apellido = "perez";
delete from clientes where apellido like "%perez%";
delete from clientes where cuit like "%0";

create table articulos(
id int auto_increment,
nombre varchar(50),
precio double,
stock integer,
primary key (id)
);

insert into articulos (nombre, precio, stock) values ('cable de red',30,50);
insert into articulos (nombre, precio, stock) values ('memoria SD',140,22);
insert into articulos (nombre, precio, stock) values ('pendrive',80,28);
insert into articulos (nombre, precio, stock) values ('cargador celular',60,18);
insert into articulos (nombre, precio, stock) values ('teclado logitech',180,13);
insert into articulos (nombre, precio, stock) values ('pila',40,13);
insert into articulos (nombre, precio, stock) values ('cable USB',40,13);
insert into articulos (nombre, precio, stock) values ('cable USB',40,13);

drop table articulos;
select * from articulos;

create table empleados(
        codigo int auto_increment,
        nombre varchar(20) not null,
        apellido varchar(20) not null,
        seccion varchar(20),
        sueldo float,
        primary key (codigo)
);

insert into empleados (nombre,apellido,seccion,sueldo) values
        ('juan','perez','administracion',7200),
        ('diego','torres','ventas',3520),
        ('laura','gomez','ventas',4600),
        ('mario','lopez','produccion',4500),
        ('dario','sanchez','administracion',8600),
        ('juan','boneli','administracion',7200),
        ('diego','martinez','ventas',3520),
        ('laura','moretti','ventas',4600),
        ('sandra','lazante','produccion',4500),
        ('susana','mendez','administracion',8600);


update articulos set precio = precio * 1.2 where precio <= 50;
update articulos set precio = precio * 1.15 where precio >= 50;
update articulos set precio = round(precio, 2) where precio between 206 and 207;
update articulos set precio = precio * 0.95 where precio > 200;


select * from empleados;
truncate empleados;

insert into empleados (nombre,apellido,seccion,sueldo) values
        ('juan','perez','administracion',7200),
        ('diego','torres','ventas',3520),
        ('laura','gomez','ventas',4600),
        ('mario','lopez','administracion',4500),
        ('dario','sanchez','administracion',8600),
        ('juan','boneli','produccion',7200),
        ('diego','martinez','ventas',3520),
        ('laura','moretti','ventas',4600),
        ('sandra','lazante','produccion',4500),
        ('susana','mendez','administracion',8600);

update empleados set seccion = "produccion" where nombre = "mario" and apellido = "lópez";
update empleados set sueldo = sueldo * 1.15 where seccion = "ventas";
update empleados set sueldo = sueldo * 1.08 where sueldo < 6000 and seccion = "ventas";
delete from empleados where nombre = "susana" and apellido = "mendez";
update empleados set sueldo = sueldo * 1.04 where sueldo >= 5000;
update empleados set sueldo = sueldo * 1.08 where sueldo < 5000;

create table secciones(
	id int auto_increment,
    nombre varchar(60),
    primary key(id)
);

insert into secciones (nombre) values ("administracion"),("produccion"),("ventas");


select * from secciones;

create table empleados_2(
        codigo int auto_increment,
        nombre varchar(20) not null,
        apellido varchar(20) not null,
        seccion_id int,
        sueldo float,
        primary key (codigo)
);

truncate empleados;
insert into empleados_2 (nombre,apellido,seccion_id,sueldo) values
        ('juan','perez','administracion',7200),
        ('diego','torres','ventas',3520),
        ('laura','gomez','ventas',4600),
        ('mario','lopez','produccion',4500),
        ('dario','sanchez','administracion',8600),
        ('juan','boneli','administracion',7200),
        ('diego','martinez','ventas',3520),
        ('susana','moretti','ventas',4600),
        ('sandra','lazante','produccion',4500),
        ('susana','mendez','administracion',8600);
select * from empleados_2;
drop table empleados;
rename table empleados_2 to empleados;

select * from empleados;
truncate empleados;

insert into empleados (nombre,apellido,seccion_id,sueldo) values
        ('juan','perez',1,7200),
        ('diego','torres',3,3520),
        ('laura','gomez',3,4600),
        ('mario','lopez',2,4500),
        ('dario','sanchez',3,8600),
        ('juan','boneli',1,7200),
        ('diego','martinez',3,3520),
        ('susana','moretti',3,4600),
        ('sandra','lazante',2,4500),
        ('susana','mendez',1,8600);

# select e.nombre,e.apellido,s.nombre,e.sueldo from empleados e join secciones s ON e.seccion_id = s.id;

select * from albums WHERE user_id = (SELECT id FROM usuarios WHERE nombre = "daphne");

SELECT id FROM usuarios WHERE nombre = "daphne";

use cursomysql3;
insert into clientes (nombre,apellido) values ("Сантьяго","Гонсалес");
select * from clientes;

alter table clientes change nombre nombre varchar(60) CHARSET utf8;
alter table clientes change apellido apellido varchar(60) CHARSET ascii;




use redSocial;

select * from usuarios u right join albums a on u.id = a.user_id;

select * from usuarios, albums;

insert into albums (user_id,titulo,descripcion) values (45,"Viaje a Mesopotamia","Sólo las fotos que sacamos johnny y yo");

describe albums;

select *,max(id) from usuarios;

use cursomysql3;

select * from usuarios;

show tables;

select * from articulos;

