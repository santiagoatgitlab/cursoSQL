##########
# INSERT #
##########


# Insert con definición de campos

	# Especificando algunos campos 
    
    # Invirtiendo el orden de los campos
    
    # Que pasa cuando no insertamos nada en un campo obligatorio
    
    # Que pasa cuando intentamos insertar null en un campo obligatorio
    
    # Que pasa cuando especifico el valor de un campo auto_incremental


    
# Insert sin especificación de campos

	# Ejemplo normal
    
    # Ejemplo pasando null al campo auto_incremental
    
    # Ejemplo cuando no especifico el valor de alguno de los campos
    
    # Ejemplo cuando quiero dejar vacío uno nullable
    
	# Ejemplo cuando quiero dejar vacío uno not nullable



# Insert masivo (Ejemplo con 3 registros)
	
# Ejemplo de error de clave duplicada



##########
# DELETE #
##########

# Ejemplos de delete con where

	# id, nombre, id >, id <
    
# Ejemplo de delete sin where

# Ejemplo de insertar registros luego de haber borrado con delete (que pása con los id's)

# Ejemplo de truncate

# Ejemplo de insertar registros luego de haber borrado todo con truncate (que pása con los id's)




##########
# UPDATE #
##########

# Update con where por nombre

# Update con where por id masivo (>=)

# Update de mas de un campo al mismo tiempo 


# Update sin where

-- EJERCICIOS --

###############
# ALTER TABLE #
###############


# Drop de claves

	# ejemplo al intentar hacer drop de la primary key (error)
    
# Modify

	# eliminar atributo auto_incremental de campo id
    
# drop de primary key

# ejemplo de intentar agregar registros sin especificar id

# intentar agregar primary key en id (error)

# borrar registros con id repetidos (id = 0)

# agregar primary key al campo id

##########
# RENAME #
##########

# ejemplo de rename