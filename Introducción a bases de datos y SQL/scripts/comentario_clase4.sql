###########################
# FUNCIONES DE AGRUPACION #
###########################

# Ejemplos sin agrupar

	# funcion: max, campos: precio, stock (2 queries)

	# funcion: min, campos: precio, nombre (agregar alias) (2 queries)

	# funcion: avg, campo: precio (1 queries)

	# funcion: count, campos: *, descripcion, categoría (3 queries)

	# funcion: sum, campos: stock, precio (2 queries)

# Ejemplos agrupando con where

	# funcion: max, marcas: samsung, LG, sony

############
# GROUP BY #
############

# funciones: max(precio), count(*), sum(stock) agrupado por marca agregar alias y la columna marca (3 queries)

# funciones: max(precio), count(*), sum(stock) agrupado por categoria agregar alias y la columna categoria (3 queries)
    
##############
# SUBQUERIES #
##############

# datos de los productos de precio mínimo y máximo (2 queries)

# datos de los productos de stock mínimo y máximo (2 queries)

# datos de los productos de precios menor y mayor que el promedio (2 queries)

-- Explicar orden de las clausulas en la query
-- Ejercicios con group by. Explicar orden de las clausulas en la query
    
########
# JOIN #
########

# producto cartesiano paises continentes
    
# producto relacional paises continentes (alias de tabla requerido)
    
-- Concepto de clave foranea

# union entre las tablas paises y continentes a través de la clausula join

# union entre las tablas con join left

# union entre las tablas con join right

-- Ejercicios con join
-- Repasar orden de las clausulas, ir agregando las clausulas de a una

##########
# HAVING #
##########

# Tabla productos, categorias que tengan mas de 6 productos

# Tabla productos, categorias para los cuales no haya ningun productos de mas de 7000 pesos

# Tabla productos, categorias para los cuales no haya ningun productos de menos de 600





-- Queries con mas de una join
-- Uniones múltiples