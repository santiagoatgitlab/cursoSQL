show databases;
create database cursosql3;
use cursosql3;
show tables;
-- creamos la tabla clientes

create table clientes (
codigo integer auto_increment,
nombre varchar(20) not null,
apellido varchar(20) not null,
cuit varchar(13),
direccion varchar(50),
comentarios varchar(140),
primary key (codigo)
);

-- igresamos 5 registros en tabla clientes
insert into clientes (nombre,apellido,cuit,direccion) values ('Camila','hernández','20-31455641-1','mompox 1395');
insert into clientes (nombre,apellido,cuit,direccion) values ('Camila','pratto','23-34554346-5','apulé 145');
insert into clientes (nombre,apellido,cuit,direccion) values ('Esteban','Perez','23-24332659-1','casacuberta 156');
insert into clientes (nombre,apellido,cuit,direccion) values ('ariana','gonzález','20-31467553-6','garro 1586');
insert into clientes (nombre,apellido,cuit,direccion) values ('Elias Mariano','González','24-34023949-3','Esquel 1945 C');
insert into clientes (nombre,apellido,cuit,direccion) values ('juan','perez','23-14565443-2','peru 323');
insert into clientes (nombre,apellido,cuit,direccion) values ('diego','torres','23-37563181-4','chile 320');
insert into clientes (nombre,apellido,cuit,direccion) values ('laura','gomez','20-13546778-1','san juan 420');
insert into clientes (nombre,apellido,cuit,direccion) values ('mario','lopez','27-15665778-1','lavalle 770');
insert into clientes (nombre,apellido,cuit,direccion) values ('dario','sanchez','27-29334610-8','mexico 150');

-- 1- Crear la tabla Facturas dentro de la base de datos con el siguiente detalle:

-- Letra		char y PK
-- Numero		integer y PK
-- Fecha		date
-- Monto		double

-- PK significa Primary Key
-- observar que se esta declarando una clave primaria compuesta
-- es decir (letra,codigo)
-- cada campo por si solo no es clave, ni tampoco identifica al registro
-- pero la suma de los dos forman la clave

create table facturas(
letra char(1),
numero integer,
fecha date,
monto double,
primary key (letra,numero)
);


-- 2- Crear la tabla Articulos dentro de la base de datos con el siguiente detalle:

-- Codigo		integer y PK
-- Nombre 		varchar (50)
-- Precio		double
-- Stock		integer

create table articulos(
codigo integer,
nombre varchar(50),
precio double,
stock integer,
primary key (codigo)
);

-- 3- Cargar 5 registros en cada tabla

insert into articulos values (1,'cable de red',30,50);
insert into articulos values (2,'memoria SD',140,22);
insert into articulos values (3,'pendrive',80,28);
insert into articulos values (4,'cargador celular',60,18);
insert into articulos values (5,'teclado logitech',180,13);
insert into articulos values (6,'pila',40,13);
insert into articulos values (7,'cable USB',40,13);
insert into articulos values (8,'cable USB',40,13);

insert into facturas values ('a',0001,'2011/10/18',500);
insert into facturas values ('a',0002,'2011/10/18',2500);
insert into facturas values ('b',0003,'2011/10/18',320);
insert into facturas values ('b',0004,'2011/10/18',120);
insert into facturas values ('b',0005,'2011/10/18',560);
-- inserto un registro con la fecha actual
insert into facturas values ('c',0006,curdate(),300);

select letra from facturas;
select letra,monto from facturas;
select monto from facturas;
select monto * 2 from facturas;
select monto, monto * 1.21 as "monto + iva" from facturas;

select * from clientes where nombre = 'camila';
select * from articulos where precio > 60;
select * from articulos where precio < 60;
select * from articulos where precio >= 60;
select * from articulos where precio <= 60;
select * from articulos where precio != 40;

select * from clientes where nombre = "camila" and apellido = "pratto";
select * from clientes where nombre = "camila" or apellido = "hernandez";

select * from clientes;

select * from articulos where precio >= 60 and precio <= 120;
select * from articulos where precio < 60 and precio > 120;

drop table if exists clientes;
create table clientes(
	nombre VARCHAR(30) NOT NULL,
	apellido VARCHAR(40) NOT NULL,
	edad INT,
    domicilio VARCHAR(60),
    genero CHAR(1)
);

insert into clientes (nombre) values ("ramiro");

alter table clientes drop edad;z
alter table clientes add edad int;
insert into clientes (nombre, edad, apellido) values ("santiago", 39, "rodriguez");

select * from clientes;

use cursosql3;
show tables;

create table productos(
	id int auto_increment primary key,
	nombre varchar(40) not null,
    descripcion varchar(255),
    precio float not null,
    cantidad int
);