cuando el docente ingrsa al instituto debe dirigirse al piiso 10.
Alli debe ingresar su numero de DNI para que quede su llegada registrada.
Luego en el piso 10 encontrará el aula de distancia.

Para saber cual computadora corresponde a su curso debe fijarse las iniciales del curso. En este caso, java Standard.
Al lado debe fijarse que diga el número de clase, en este caso clase 1. Y al lado el código del curso.

En participantes encontrará conectado a educacionIT de 01 a 10. En este caso somos educaciónIT 09.
Luego encontrará al docente conectado como instructor, a los alumnos conectados y luego a la Pc espejo que estará conectada con el usuario de un punto.

La pc espejo sirve para que el docente vea lo que le está compartiendo a lso alumnos. Está conectada como si fuera un alumno más.
5 minutos antes del inicio de clase, educaciónIT le pasa las funciones de presentador y organizador al instructor.
Se le pasa las funciones haciendo click con el botónt de derecho -> cambiar función -> presentador.
Nuevamente click derecho -> cambiar función -> organizador y luego aceptar.

Antes de iniciar la clase el docente debe silenciar el micrófono de cada alumno, haciendo click, aquí, en cada participante.
Es muy importante que el docente silencie los micrófonos de los alumnos para que el micrófono principal que se escuche sea el de él.

Como se indicó anteriormente 5 minutos antes del inicio de clase el usuario educaciónIT le pasa las funciones al instructor.
A veces, webex funciona mal y le pasa el balón de presentador a un alumno, como se puede ver aquí en este caso. Cuando sucede esto el docente debe arrastrar el balón hacia instructor. Luego pone que si, asegurandose de tener ambas funciones, el balón de presentador y organizador.

Al comenzar la clase el docente debe solicitar a los alumnos que los que quieran participar de la misma deben escribir en el chat en la opción todos.

Si un alumno no tiene audio de le debe indicar que vaya a la solapa de audio -> conferencia de voz integrada -> entrar en la conferencia.
Si un alumno tiene microfono puede participar de la clase levantando la mano. Si un alumno levanta la mano, al lado del alumno va a aparecer la mano en color azul y el docente debe habilitarle el micrófono para que el alumno pueda hablar. Una vez que finaliza de hablar, le vuelve a silenciar el micrófono. Si un alumno o un docente tiene la cámara prendida tiene que apagarla porque consume banda ancha. Para apagarla, debe hacer click en el icono en color azul que aparece al lado de su nombre. En este caso, aquí como la tenemos desactivada aparece en color gris. Si estuviese activada aparecería en color azul. 
El docente no puede apagarle la cámara al alumno, sólo el alumno puede desactivar su propia cámara.

Como 5 minutos antes del inicio de clase se le pasó al instructor las funciones, el usuario educaciónIT ya no puede agregar a un alumno. En caso de que un alumno no haya recibido el link, el usuario educacionIT le pedirá al instructor que lo agregue. En el chat, educacionIT le enviará un mensaje al instructor para que sólo lo lea el docente diciéndole "lo agregás a" y al lado copiará el mail del alumno y luego educacionIT enviará el mensaje. Cuando el usuario educacionIT le solicita al docente por mensaje privado en el chat que agregue a un alumno, como en este caso, el docente debe copiar el mail del alumno, ir a la solapa de inicio rápido y donde dice invitar y enviar recordatorio, hace click -> correo electónico -> pega el mail y enviar. y luego click en hecho.
Muy pocas veces sucede que se conecta un alumno que no está inscripto en el curso o que no está identificado con nombre y apellido. Si educacionIT le solicita al docente que debe expulsar a un alumno, el docente no debe comentarle nada al alumno ya que educacionIT habló por privado con él.
En este caso educacionIT le solicita por mensaje privado al instructor que expulse a "alumno", el docente debe hacer click en "alummno" -> participante -> expulsar. y luego poner que si.

Los alumnos tienen break al igual que los cursos presenciales. El break es de 15 minutos y antes de salir al break el docente debe escribir en la pizarra "estamos en break, volvemos a tal hora" así los alumnos que se conectan tarde, están al tanto. La clase de distancia se graba porque al día siguiente se sube la grabación al alumni por 7 días. Al comenzar la clase el docente debe poner "grabar" y asegurarse que aquí diga grabando. Al salir al break pone "pausa", al volver del break "grabar" y luego al terminar la clase "pausa". 

La primer clase los alumnos no tienen el curso activado en el alumni, hasta que se carga el primer presente en el sistema, que es a la hora de haber iniciado la clase. Si el docente quiere pasarle material a los alumnos, puede ir a archivo -> transferir -> compartir archivo -> seleccionar el archivo que quiere compartir -> y luego abrir.
También el docente puede utilizar la pizarra. Puede abrir todas las pizarras que quiera. En la pizarra puede escribir, puede colocar flechas, puede hacer cuadros, lo que quiera. La pizarra no es necesario que la guarde, aqui le pone desea continuar, si -> desea guardar el documento -> no.
Ya que la clase está en vivo y en directo.

Lo mas importante en esta modalidad es la compartición de pantalla por parte del docente. El docente debe hacer click en compartición, aquí y luego todas las funciones se vienen acá arriba. Lo mas importante es que el docente abra: participantes, chat y grabadora. Si el docente quiere detener la compartición, hace click aqui y vuelve a la pantalla principal. 

Para terminar la clase el docente pone pausa -> va a la solapa de inicio rápido -> abandonar la reunión -> click en guardar -> y terminar la reunion.

