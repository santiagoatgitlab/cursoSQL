var inscripcionCursos = function() {

    $.widget( "custom.combobox", {
        _create: function() {
            this.wrapper = $( "<span>" )
                .addClass( "custom-combobox" )
                .insertAfter( this.element );

            this.element.hide();
            this._createAutocomplete();
            this._createShowAllButton();
        },

        _createAutocomplete: function() {
            var selected = this.element.children( ":selected" ),
                value = selected.val() ? selected.text() : "";

            this.input = $( "<input>" )
                .appendTo( this.wrapper )
                .val( value )
                .attr( "title", "" )
                .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )

                .autocomplete({
                    delay: 0,
                    minLength: 0,
                    source: $.proxy( this, "_source" )
                })

                .tooltip({
                    tooltipClass: "ui-state-highlight"
                });

            this._on( this.input, {
                autocompleteselect: function( event, ui ) {
                    ui.item.option.selected = true;
                    this._trigger( "select", event, {
                        item: ui.item.option
                    });
                },
                click: function(){
                    if(selected.val() == '0') {
                        this.input.val('');
                    }
                },
                autocompletechange: "_removeIfInvalid"
            });
        },

        _createShowAllButton: function() {
            var input = this.input,
                wasOpen = false;

            $( "<a>" )
                .attr( "tabIndex", -1 )
                .attr( "title", "Mostrar todas las opciones" )
                .tooltip()
                .appendTo( this.wrapper )

                .button({
                    icons: {
                        primary: "ui-icon-triangle-1-s"
                    },
                    text: false
                })

                .removeClass( "ui-corner-all" )
                .addClass( "custom-combobox-toggle ui-corner-right" )
                .mousedown(function() {
                    wasOpen = input.autocomplete( "widget" ).is( ":visible" );
                })

                .click(function() {
                    input.focus();

                    // Close if already visible
                    if ( wasOpen ) {
                        return;
                    }

                    // Pass empty string as value to search for, displaying all results
                    if($("#selectCareer").val() != '') {
                        input.autocomplete("search", "");
                    }
                });
        },

        _source: function( request, response ) {
            var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
            response( this.element.children( "option" ).map(function() {
                var text = $( this ).text();
                if ( this.value && ( !request.term || matcher.test(text) ) )
                    return {
                        label: text,
                        value: text,
                        option: this
                    };
            }) );
        },

        _removeIfInvalid: function( event, ui ) {

            // Selected an item, nothing to do
            if ( ui.item ) {
                return;
            }

            // Search for a match (case-insensitive)
            var value = this.input.val(),
                valueLowerCase = value.toLowerCase(),
                valid = false;
            this.element.children( "option" ).each(function() {
                if ( $( this ).text().toLowerCase() === valueLowerCase ) {
                    this.selected = valid = true;
                    return false;
                }
            });

            // Found a match, nothing to do
            if ( valid ) {
                return;
            }

            // Remove invalid value
            this.input
                .val( "" )
                .attr( "title", value + " No es una opción valida" )
                .tooltip( "open" );
            this.element.val( "" );
            this._delay(function() {
                this.input.tooltip( "close" ).attr( "title", "" );
            }, 2500 );
            this.input.data( "ui-autocomplete" ).term = "";
        },

        _destroy: function() {
            this.wrapper.remove();
            this.element.show();
        }
    });

    $("#selectCareer").on('change',function(){
        $.ajax({
            'url': '/js/traerCursos.php',
            'data': 'career_id=' + $(this).val(),
            'type': 'GET',
            'complete': 'javascriot: void(0)',
            'success': function (rta) {
                $('#selectCourse').html(rta);
            }
        });
    })

    $( "#selectCourse" ).combobox();
    $( "#toggle" ).click(function() {
        $( "#course_id" ).toggle();
    });
    $('.ui-autocomplete-input').css('width','390px');
};

var keepSessionAlive = function(){

    function sessionAlive(){
        $.ajax({
            'url': '/user/keepSessionAlive',
            'type': 'GET',
            'complete': 'javascriot: void(0)',
            'success': function (rta) {
            }
        });
    }
    setInterval(sessionAlive,500000);

}

var muro = function(){
    $('#selectCourseCalendar').on('change', function(){
        window.location.href='/coursecalendar/muro/cc/' + $('#selectCourseCalendar').val();
    });

    function verificarNuevosMsj(){
        $.ajax({
            'url': '/coursecalendar/actualizarmuro/',
            'data': {
                cc: $('#id_course_calendar').val()
            },
            'type': 'POST',
            'complete': 'javascript: void(0)',
            'success': function (rta) {
                obj = JSON.parse(rta);
                if (obj.ok) {
                    //$("#contentEntradas").prepend("<div class='itemEntrada' style='background-image:url(" + $("#avatar_url").val() + "); background-size: 46px 46px;'><h4>"+$('#nombre_estudiante').val() +"</h4> <span>Hace un instante</span><div>" + mensaje + "</div></div>");
                }else{
                    alert(obj.msg);
                }

            }
        });
    }
    verificarNuevoMsj();
    //setInterval(verificarNuevosMsj,3000);
};

var checkMail = function(){
    $('.mensaje').on('click', function(){
        var active = $(this).hasClass('active');
        $('.mensaje').removeClass('active');
        var unread = $(this).hasClass('unread');
        if(unread == true){
            var msg = $(this);
            $.ajax({
                'url': '/user/marcarmensajeleido',
                'data': {
                    message_id: $(this).data('message_id')
                },
                'type': 'POST',
                'complete': 'javascript: void(0)',
                'success': function (rta) {
                    obj = JSON.parse(rta);
                    if (obj.ok) {
                        if(obj.unread_messages > 0){
                            $('.mensaje-count').html('<span>'+obj.unread_messages+'</span>');
                        }else{
                            $('.mensaje-count').remove();
                        }

                        msg.removeClass('unread');
                    }

                }
            });
        }
        if(active){
            $(this).removeClass('active');
        }else{
            $(this).addClass('active');
        }

    });
};

var deleteMessage = function(){
    $('.icon-trash-o').on('click', function(){
        var r = confirm("El mensaje será eliminado.");
        if (r == true) {
            var message_id = $(this).data('message_id');
            var unread = $(this).hasClass('unread');
            $.ajax({
                'url': '/user/eliminarmensaje',
                'data': {
                    message_id: message_id
                },
                'type': 'POST',
                'complete': 'javascript: void(0)',
                'success': function (rta) {
                    obj = JSON.parse(rta);
                    if (obj.ok) {
                        $('#msj_'+message_id).remove();

                        if(unread == true){
                            var msg = $(this);
                            if(obj.unread_messages > 0){
                                $('.mensaje-count').html('<span>'+obj.unread_messages+'</span>');
                            }else{
                                $('.mensaje-count').remove();
                            }
                        }
                        if(obj.last_messages > 0) {
                            $('.mensaje-count-int').html('(' + obj.last_messages + ')');
                        }else {
                            $('.mensaje-count-int').remove();
                        }
                    }

                }
            });
            $('#header-msj-'+message_id).remove();
        }
    });
};


var mensajeMuro = function(){
    $('#enviarMensajeMuro').on('click', function(){
        var mensaje = $('#comentario').val();
        if(mensaje == ''){
            alert('Por favor, agrega un comentario');
            return false;
        }
        $.ajax({
            'url': '/coursecalendar/nuevomensajemuro',
            'data': {
                id_course_calendar: $('#id_course_calendar').val(),
                sucursal_act: $('#sucursal_act').val(),
                mensaje: mensaje
            },
            'type': 'POST',
            'complete': 'javascript: void(0)',
            'success': function (rta) {
                obj = JSON.parse(rta);
                if (obj.ok) {
                    $('#comentario').val('');
                    $("#contentEntradas").prepend("<div class='itemEntrada' style='background-image:url(" + $("#avatar_url").val() + "); background-size: 46px 46px;'><h4>"+$('#nombre_estudiante').val() +"</h4> <span>Hace un instante</span><div>" + mensaje + "</div></div>");
                }

            }
        });
        return false;
    });
};

var alert = function(txtalert,titlealert){
    var base = "<div id='alert-container' class='alert-container'>"
        +"<div class='header'>"
        +"<h1></h1>"
        +"<span class='icon-times'></span>"
        +"</div>"
        +"<div id='alert-content' class='alert-content'>"
        +"<span class='icon-exclamation-circle'></span>"
        +"<p></p>"
        +"<a href='#'>Aceptar</a>"
        +"</div>"
        +"</div>";


    if(!$('#alert-container').length){
        $(base).appendTo('body');
    }
    $('#alert-container').fadeIn('fast');

    if(typeof(titlealert) == 'undefined' || titlealert == ''){
        titlealert = 'Alerta';
    }
    $('#alert-container .header h1').text(titlealert);
    $('#alert-content p').text(txtalert);

    $('html, body').css({
        'overflow': 'hidden',
        'height': '100%'
    });
    $("#alert-content a,#alert-container .icon-times").on('click',function(){
        $('#alert-container').fadeOut('fast');
        $('html, body').css({
            'overflow': 'auto',
            'height': 'auto'
        });
    });
};

var popform = function(idelem,title){
    var base = "<div class='alert-container' id='popform-container'>"
        +"<div class='header'>"
        +"<h1></h1>"
        +"<span class='icon-times'></span>"
        +"</div>"
        +"<div id='popform-content' class='alert-content'>"
        +"<p></p>"
        +"</div>"
        +"</div>";


    if(!$('#popform-container').length){
        $(base).appendTo('body');
    }
    $('#popform-container').fadeIn('fast');

    if(typeof(title) == 'undefined' || title == ''){
        title = 'Reporte de errores';
    }
    $('#popform-container .header h1').text(title);
    $('#popform-content p').append($('#report'));

    $('html, body').css({
        'overflow': 'hidden',
        'height': '100%'
    });
    $("#popform-content a,#popform-container .icon-times").on('click',function(){
        $('#popform-container').fadeOut('fast');
        $('html, body').css({
            'overflow': 'auto',
            'height': 'auto'
        });
    });
};


var loginPassword = function(){
	$('.link-pass-reset').on('click',function(){
		$('.form-mail-password').addClass('show');
	});
};
var introLoad = function(){
	$('.load-intro-loop-img').addClass('active-name');
	$('.intro-loading').on('click',function(){
		$(this).remove();
	});
};
var videoProgress = function(){
	$('.container-video-progress').each(function(){
		var parentHeight = $(this).parent().height();
      	$(this).height(parentHeight);
	});
};

function cargarResultadosExamen(idExamen, idUser, idClass) // Forma de definir m�todos STAMENT
{
    var xhr = getXHR();
    xhr.open("GET","/js/cargarResultadosExamen.php?examenId="+idExamen+"&userId="+idUser+"&classId="+idClass+"&sucursalId="+idSucursal);
    xhr.onreadystatechange = function () // Forma de definir m�todos EXPRESSION
    {
        if(xhr.readyState == 4)
        {
            if(xhr.status == 200)
            {
                $('.contenido-dinamico').html(xhr.responseText);
            }
        }

    }
    xhr.send(null);
}

var loadContent = function(){

    $(document).on({
	    ajaxStart: function() { $('.contenedor-contenido-dinamico').addClass("loading");    },
	    ajaxComplete: function() { $('.contenedor-contenido-dinamico').removeClass("loading"); }
	});

    // De aca para abajo ya no se usa aparentemente en el request /clases/videos/
    // porque se carga directamente
    // pero se usa en el request /clases/indice para traer el menu

        var url = '';
        var type = 'POST';
        var paramEx = '';
        var paramUserId = '';
        var paramSucursalId = '';

        $.ajax({
            'url': '/clases/ajaxcargarcontenido',
            'data': {
                classId: $("#idClass").val(),
                courseId: $("#idCourse").val()
            },
            'type': 'POST',
            'complete': 'javascript: void(0)',
            'success': function (rta) {
                try {
                    obj = JSON.parse(rta);
                    if (obj.ok) {

                        $('.contenido-dinamico').html(obj.html);

                        videoProgress();
                        $(window).resize(function () {
                            videoProgress();
                        });

                        $('.contenido-caducado').click(function(){
                            alert("Esta sección se encuentra momentáneamente deshabilitada. El tiempo de expiración de los cursos es de " + obj.tiempo_caducar_curso + " meses. Pero si vuelves a tomar un nuevo curso en el instituto, se activaran todos los anteriores.",'Caducidad de contenido');
                        });
                    }
                } catch (e) {
                    location.href="/";
                    //$('.contenido-dinamico').html(rta);
                }
            }
        });


};


var changeFormIcon = function(clase){
	$(".form-icon").empty().append("<span class="+clase+"></span>").hide().fadeIn(1000);
};

var changeDynamicForm = function(){
	var dinamicInputs = "";

	$( ".tema-formulario select" )
	.change(function () {
	    var dinamicInputs = "";
	    $( ".tema-formulario select option:selected" ).each(function() {
	      dinamicInputs += $( this ).val();
	    });

            if(dinamicInputs == 'solucion' || dinamicInputs == 'sugerencia'){
                dinamicInputs = 'solucion-sugerencia';
            }
            if($('.dinamic-inputs > div').attr('id') != dinamicInputs) {
                $('.dinamic-inputs').slideUp(200).slideDown(200);
                $('.dynamicInputs').addClass('hide');
                $('.dinamic-inputs > div').replaceWith($('#' + dinamicInputs)).appendTo('body');
                $('#' + dinamicInputs).removeClass('hide');

                switch (dinamicInputs) {
                    case "solucion-sugerencia":
                        changeFormIcon('icon-tools');
                        break;
                    case "error-tecnico":
                        changeFormIcon('icon-error');
                        break;
                    case "mail-director":
                        changeFormIcon('icon-director');
                        break;
                    default:
                        changeFormIcon('');
                        break;
                }
            }
	  });

};

var mensajePersonal = function() {
    $('.icon-close,.separador').on('click', function () {
        $('.container-mensaje-personal,.divider').hide(500);
    });
    $('.enviarMsjPersonal').on('click', function () {
        window.scrollTo(0, 0);
        $('.container-mensaje-personal,.divider').fadeIn(500);

        nombreAl = $('#user_'+ $(this).data('user_id')+ ' .nombre_alumno').html();
        $('.nombre-alumno').html(nombreAl);
        $('#destinatario_id').val($(this).data('user_id'));
        $('#destinatario_sucursal').val($(this).data('sucursal_id'));
    });

    $('.responderMsjPersonal').on('click', function () {
        window.scrollTo(0, 0);
        $('.container-mensaje-personal,.divider').fadeIn(500);

        $('.nombre-alumno').html($(this).data('nombre'));
        $('#destinatario_id').val($(this).data('user_id'));
        $('#destinatario_sucursal').val($(this).data('sucursal_id'));
    });

    $('.btn-continuar').on('click',function(){

        if ($('#contenido-mensaje').val() == '') {
            alert('Por favor ingresá el contenido del mensaje');
            return false;
        }
        $.ajax({
            'url': '/user/ajaxnuevomensaje',
            'data': {
                user_id: $('#destinatario_id').val(),
                sucursal_id: $('#destinatario_sucursal').val(),
                mensaje: $('#contenido-mensaje').val()
            },
            'type': 'POST',
            'complete': 'javascript: void(0)',
            'success': function (rta) {
                $('#contenido-mensaje').val('');
            }
        });
        $('.container-mensaje-personal,.divider').hide(500);

    });
};

var loadFormCommunity = function(){
	$('.icon-close,.separador').on('click',function(){
		$('.container-facebook-community,.divider').hide(500);
	});
	$('#btn-facebook-commnunity').on('click',function(){
		window.scrollTo(0, 0);
		$('.container-facebook-community,.divider').fadeIn(500);
	});

    $('#btn-facebook-commnunity-dash').on('click',function(){
        window.scrollTo(0, 0);
        $('.nombre-comunidad').html($(this).data('nombre'));
        $('#comunidad_id').val($(this).data('comunidad_id'));
        $('#usuariofb').val($(this).data('usuario_fb'));
        $(".container-facebook-community .main-content").append($('#main1'));
        $('#main1').removeClass('hide');
        $('.container-facebook-community,.divider').fadeIn(500);
    });

    var paso = 1;

    $('.btn-continuar').on('click',function(){

        if(paso == 1) {
            if ($('#usuariofb').val() == '') {
                alert('Por favor ingresá tu usuario de Facebook');
                return false;
            }
            $.ajax({
                'url': '/user/ajaxguardarusuariofb',
                'data': {
                    usuariofb: $('#usuariofb').val(),
                    comunidad_id: $('#comunidad_id').val()
                },
                'type': 'POST',
                'complete': 'javascript: void(0)',
                'success': function (rta) {
                    paso++;
                    $(".container-facebook-community").attr('id','paso2');
                    $('#main1').addClass('hide');
                    $("body").append($('#main1'));
                    $(".container-facebook-community .main-content").append($('#main2'));
                    $('#main2').removeClass('hide');
                }
            });
        }else if(paso == 2) {
            paso++;
            $(".container-facebook-community").attr('id','paso'+paso);
            $('#main2').addClass('hide');
            $("body").append($('#main2'));
            $(".container-facebook-community .main-content").append($('#main3'));
            $('#main3').removeClass('hide');
        }else{
            $('.container-facebook-community,.divider').hide(500);
        }

    	if(paso >= 4){paso=0;}
    });

};

var popInformacionCurso = function(){
    $('.icon-close,.separador').on('click',function(){
        $('.container-informacion-curso,.divider').hide(500);
    });
    $('.icon-information').on('click',function(){
        $('.container-informacion-curso .center-information .header').html($(this).parent().data('course'));
        $('.container-informacion-curso .center-information .fecha-inicio').html($(this).parent().data('f-inicio'));
        $('.container-informacion-curso .center-information .fecha-fin').html($(this).parent().data('f-fin'));
        $('.container-informacion-curso .center-information .docente').html($(this).parent().data('docente'));
        $('.container-informacion-curso .center-information .turno').html($(this).parent().data('turno'));
        $('.container-informacion-curso .center-information .horario').html($(this).parent().data('horario'));
        $('.container-informacion-curso .center-information .dia').html($(this).parent().data('dia'));
        $('.container-informacion-curso').fadeIn(500);

        $.ajax({
            'url': '/user/ajaxguardarusuariofb',
            'data': {
                usuariofb: $('#usuariofb').val(),
                comunidad_id: $('#comunidad_id').val()
            },
            'type': 'POST',
            'complete': 'javascript: void(0)',
            'success': function (rta) {
                paso++;
                $(".container-facebook-community").attr('id','paso2');
                $('#main1').addClass('hide');
                $(".main-content").html($('#main2'));
                $('#main2').removeClass('hide');
            }
        });

    });

};

var videoMenu = function(){
    $(".ver-contenido").click(function(){
        if($('.videos-menu').width() == 0) {
            var h = $('.contenedor-ppal').height();
            $('.videos-menu').css('height', h).css('width', '320px');
            $('.contenedor-ppal').css('display','inline-block').css('width', 'calc(100% - 400px)').css('marginLeft','40px');
            $(".ver-contenido").html('<span class="icon-times"> </span>Ocultar Contenido');
        }else{
            $('.videos-menu').css('width', '0px');
            $('.contenedor-ppal').css('display','block').css('width', '90%').css('margin','0 auto');
            $(".ver-contenido").html('<span class="icon-menu"> </span>Ver Contenido');
        }
    });

};


//Esta funcion no estaba funcionando con un click bind de JQuery
var mobileNavDropdown = function(){
	$('.mobile-clase-actual').on('click',function(){
		console.log('click');
        $('.nav-clases-mobile').toggleClass('drop-active');
        $('.drop').slideToggle('fast');
	});
};


var video = function(){
   var containerPlayerHeight = $('.container-player').height();
   $('.container-videos-page').height(containerPlayerHeight);
};

var playMainVideo = function(src){
    if(document.getElementById('openingVideo')) {
        document.getElementById('openingVideo').style.display = 'none';
    }
    document.getElementById('mainVideo').style.display='block';
    document.getElementById('mainVideo').play();
};

var replayMainVideo = function(src){
    if(document.getElementById('closingVideo')) {
        document.getElementById('closingVideo').style.display = 'none';
    }
    document.getElementById('videoOverlay').style.display='none';
    playMainVideo(src);
};
var playClosingVideo = function(){
        document.getElementById('mainVideo').style.display='none';
        document.getElementById('closingVideo').style.display='block';
        document.getElementById('closingVideo').play();
    };
var mostrarOpcionesVideo = function(){
    document.getElementById('videoOverlay').style.display='block';
};
