	function menuDesplega(opcion) {
		var menuDesplegable = false;
		if(document.getElementById(opcion)){
			document.getElementById(opcion).style.visibility = 'visible';
			var menuDesplegable = true;						
		}
		
		//var lnk = opcion+"Lnk";
		//var div = opcion+"Div";
		
		$("#"+opcion).css('background-color','#FFF');
		$("#"+opcion).css('color','#416fb1');
		$("#"+opcion).css('border-top','1px solid #416fb1');
		$("#"+opcion).css('border-left','1px solid #416fb1');
		$("#"+opcion).css('border-right','1px solid #416fb1');
		$("#"+opcion).css('display','inline');
		if(menuDesplegable == false){
			$("#"+opcion).css('border-bottom','1px solid #416fb1');
		}else{
			$("#"+opcion).css('border-bottom','0px solid #416fb1');
		}
	}
	
	function menuLibera(opcion) {
		if(document.getElementById(opcion)){
			document.getElementById(opcion).style.visibility = 'hidden';
		}
		
		//var lnk = opcion+"Lnk";
		//var div = opcion+"Div";
		
		$("#"+opcion).css('background-color','');
		$("#"+opcion).css('color','');
		$("#"+opcion).css('border','');
		$("#"+opcion).css('display','none');
	}